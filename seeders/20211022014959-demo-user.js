'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('users', [
      {
        id: 1,
        name: 'user1',
        email: 'user1@gmail.com',
        created_at: new Date(),
        updated_at:new Date(),
      },
      {
        id: 2,
        name: 'user2',
        email: 'user2@gmail.com',
        created_at: new Date(),
        updated_at:new Date(),
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
  }
};
